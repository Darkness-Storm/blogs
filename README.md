# blogs
Задание:

Реализовать бэкенд с минимальным фронтендом (можно на голом HTML):
Имеется база стандартных пользователей Django (добавляются через админку,
регистрацию делать не надо).
У каждого пользователя есть персональный блог. Новые создавать он не может.
Пост в блоге — элементарная запись с заголовком, текстом и временем создания.
Пользователь может подписываться (отписываться) на блоги других пользователей
(любое количество).
У пользователя есть персональная лента новостей, в которой в обратном
хронологическом порядке выводятся посты из блогов, на которые он подписан.
Пользователь может помечать посты в ленте прочитанными.
При добавлении/удалении подписки содержание ленты меняется
(при удалении подписки пометки о "прочитанности" сохранять не нужно).
При добавлении поста в ленту — подписчики получают почтовое уведомление
со ссылкой на новый пост.
Изменение содержания лент подписчиков(и рассылка уведомлений) должно
происходить как при стандартной публикации поста
пользователем через интерфейс сайта, так при добавлении/удалении
поста через админку.

Техника:
Python 3.x, Django 1.10.х, Postgresql или SQLite.
Проект должен быть на гитхабе и отражать процесс разработки.
Реализовать на Class-based views.

Срок выполнения 1-2 дня.
Результат выложить на github или bitbucket.


Blogs is a simple Django app for bloging.

Quick start

    Add "blogs" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [ ... 'blogs', ]

    Include the blogs URLconf in your project urls.py like this::

    url(r'^blogs/', include('blogs.urls')),

    Run python manage.py migrate to create the blogs models (you'll need to run the command 'python manage.py makemigrations').

    Start the development server and visit http://127.0.0.1:8000/admin/ to create a blog and posts (you'll need the Admin app enabled).

    Visit http://127.0.0.1:8000/blogs/ to participate in the blogs.
    Run `python manage.py test blogs` to run tests.