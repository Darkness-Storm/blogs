from datetime import datetime

from django.shortcuts import get_object_or_404
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, FormMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, Sum, Case, When, IntegerField
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist

from blogs.models import Blog, Post
from blogs.forms import AddCommentForm


# Create your views here.


def paging(queryset, number_page, count_page=10):
    paginator = Paginator(queryset, count_page)
    try:
        page_queryset = paginator.page(number_page)
    except PageNotAnInteger:
        page_queryset = paginator.page(1)
    except EmptyPage:
        page_queryset = paginator.page(paginator.num_pages)
    return page_queryset


class IndexView(generic.ListView):
    template_name = 'blogs/index.html'
    context_object_name = 'list'

    def get_queryset(self):
        us = self.request.user
        if us.is_authenticated():
            # the list of posts subscriptions with mark read
            list_post = Post.objects.filter(
                blog__subscribers=us).annotate(
                on_read=Sum(
                    Case(
                        When(
                            users=us,
                            then=1),
                        default=0,
                        output_field=IntegerField()))).order_by(
                'on_read',
                '-created')
            page = self.request.GET.get('page')
            return paging(list_post, page, count_page=20)
#            return list

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        # a list of popular blogs - all the more subscriptions
        context['blog_list'] = Blog.objects.all().annotate(
            count_sub=Count('subscribers')).order_by('-count_sub')[0:5]
        if self.request.user.is_authenticated():
            # if the user is logged in he has access to your blog
            # with the last post
            try:
                context['my_blog'] = Blog.objects.get(user=self.request.user)
                context['my_post'] = Post.objects.filter(
                    blog__user=self.request.user).order_by('-created')[0:1]
            except ObjectDoesNotExist:
                no_blog = 'Вы еще не создали свой блог. '
                context['no_blog'] = no_blog
        return context


class PopularBlog (generic.ListView):
    template_name = 'blogs/popular.html'
    context_object_name = 'list'

    def get_queryset(self):
        list_post = Blog.objects.all().annotate(
            count_sub=Count('subscribers')).order_by('-count_sub')
        page = self.request.GET.get('page')
#        list = paging(list_post, page)
        return paging(list_post, page)


class DetailView(LoginRequiredMixin, generic.DetailView):
    model = Blog

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        blog = get_object_or_404(Blog, pk=int(self.kwargs.get('pk')))
        us = self.request.user
        if us.is_authenticated and us in blog.subscribers.all():
            subscribed = True
        else:
            subscribed = False
        context['subscribed'] = subscribed
        list_post = Post.objects.filter(
            blog=blog).annotate(
            on_read=Sum(
                Case(
                    When(
                        users=us,
                        then=1),
                    default=0,
                    output_field=IntegerField()))).order_by('-created')
        page = self.request.GET.get('page')
#        list = paging(list_post, page)
        context['list_post'] = paging(list_post, page)
        return context

    def get_object(self):
        instance = super(DetailView, self).get_object()
        if self.request.user.is_authenticated():
            if instance.user == self.request.user:
                self.template_name = 'blogs/my_blog.html'
            else:
                self.template_name = 'blogs/detail.html'
        return instance


class PostDetailView(LoginRequiredMixin, FormMixin, generic.DetailView):
    model = Post
    template_name = 'blogs/post_detail.html'
    form_class = AddCommentForm

    def get_object(self):
        instance = super(PostDetailView, self).get_object()
        # mark view
        instance.users.add(self.request.user.id)
        return instance

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        post = get_object_or_404(Post, pk=int(self.kwargs.get('pk')))
#        user_is_authenticated = self.request.user.is_authenticated()
        # if the link to edit blog
        if self.request.user == post.blog.user:
            updated = True
        else:
            updated = False
        context['updated'] = updated
        return context

    def get_success_url(self):
        return reverse('blogs:post-detail', args=[self.kwargs.get('pk')])

    def post(self, request, *args, **kwargs):
        # processing of forms for review
        if not request.user.is_authenticated():
            return HttpResponseForbidden()
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            form.instance.user = self.request.user
            form.instance.created = datetime.now()
            post = Post.objects.get(pk=int(self.kwargs.get('pk')))
            form.instance.post = post
            form.save()
        return HttpResponseRedirect(
            reverse('blogs:post-detail', args=[self.kwargs.get('pk')]))


class BlogAdd(LoginRequiredMixin, CreateView):
    model = Blog
    fields = ['title']
    template_name = 'blogs/blog_add.html'
    login_url = '/login/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(BlogAdd, self).form_valid(form)


class BlogUpdate(LoginRequiredMixin, UpdateView):
    model = Blog
    fields = ['title']
    template_name = 'blogs/blog_update.html'
    login_url = '/login/'

    def get_queryset(self):
        # edit only your blog (not available for direct link)
        return Blog.objects.filter(user=self.request.user)


class PostAdd(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'body']
    exclude = ['created', 'blog', 'updated', 'users']
    template_name = 'blogs/post_add.html'
    login_url = '/login/'

    def form_valid(self, form):
        form.instance.blog = self.request.user.user_blogs
        return super(PostAdd, self).form_valid(form)


class PostUpdate(LoginRequiredMixin, UpdateView):
    # don't think I need this view
    # when creating a new post is the newsletter, but by the time the user
    # coming to view the content of the post can be very different
    # added as a test

    model = Post
    fields = ['title', 'body']
    exclude = ['created', 'blog', 'updated', 'users']
    template_name = 'blogs/post_update.html'
    login_url = '/login/'

    def get_queryset(self):
        # edit only their own posts (not available for direct link)
        blog = self.request.user.user_blogs
        posts = Post.objects.filter(blog=blog)
        return posts

    def form_valid(self, form):
        form.instance.updated = datetime.now()
        return super(PostUpdate, self).form_valid(form)


@login_required
def add_subscription(request, pk):
    if request.method == 'POST':
        blog = get_object_or_404(Blog, pk=pk)
        blog.subscribers.add(request.user)
        return HttpResponseRedirect(reverse('blogs:index'))


@login_required
def delete_subscription(request, pk):
    if request.method == 'POST':
        blog = get_object_or_404(Blog, pk=pk)
        blog.subscribers.remove(request.user)
        return HttpResponseRedirect(reverse('blogs:index'))
