# coding: utf-8
from django.conf.urls import url

from . import views

app_name = 'blogs'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^popular/$', views.PopularBlog.as_view(), name='popular'),
    url(r'^(?P<pk>[0-9]+)/subscribe/$',
        views.add_subscription, name='subscribe'),
    url(r'^(?P<pk>[0-9]+)/del_subscribe/$',
        views.delete_subscription, name='del_subscribe'),
    url(r'^post/(?P<pk>[0-9]+)/$',
        views.PostDetailView.as_view(),
        name='post-detail'),
    url(r'^post/(?P<pk>[0-9]+)/update/$',
        views.PostUpdate.as_view(),
        name='post-update'),
    url(r'post/add/$', views.PostAdd.as_view(), name='post-add'),
    url(r'add/$', views.BlogAdd.as_view(), name='blog-add'),
    url(r'(?P<pk>[0-9]+)/update/$',
        views.BlogUpdate.as_view(),
        name='blog-update'),
]
