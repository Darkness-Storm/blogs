from django.contrib import admin
from .models import Blog, Post, Comment
# Register your models here.


class PostInline(admin.StackedInline):
    model = Post
    extra = 1


class CommentInline(admin.StackedInline):
    model = Comment
    extra = 5


class BlogAdmin(admin.ModelAdmin):
    list_display = ('user', 'title')
    inlines = [PostInline]


class PostAdmin(admin.ModelAdmin):
    list_display = ('blog', 'title', 'body', 'created')
    list_filter = ['created', 'blog']
    inlines = [CommentInline]


admin.site.register(Blog, BlogAdmin)
admin.site.register(Post, PostAdmin)
