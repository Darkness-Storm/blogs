from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User, AnonymousUser
from django.core.urlresolvers import reverse

from blogs.models import Blog, Post, Comment
from blogs.views import IndexView
# Create your tests here.


class TestView (TestCase):
    fixtures = ['test_blogs.json']

    def setUp(self):
        self.auth_user = User.objects.get(pk=2)
        self.factory = RequestFactory()

    def test_index_view_anonymous_user(self):
        request = self.factory.get(reverse('blogs:index'))
        request.user = AnonymousUser()
        response = IndexView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['list'], None)
        self.assertNotIn(str(response.context_data), 'my_post')
        self.assertNotIn(str(response.context_data), 'my_blog')
        self.assertIsNotNone(response.context_data['blog_list'])

    def test_index_view_authenticated_user(self):
        request = self.factory.get(reverse('blogs:index'))
        request.user = self.auth_user
        response = IndexView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.context_data['list'])
        self.assertIsNotNone(response.context_data['my_post'])
        self.assertIsNotNone(response.context_data['my_blog'])
        self.assertEqual(
            str(response.context_data['my_blog']), 'Блог ' + self.auth_user.username)
        self.assertIsNotNone(response.context_data['blog_list'])
