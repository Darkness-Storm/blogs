from django.db import models
from django.core.urlresolvers import reverse
from django.core.mail import send_mass_mail
# Create your models here.
from django.contrib.auth.models import User


class Blog (models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='user_blogs')
    title = models.CharField(max_length=255, verbose_name='Заголовок блога')
    subscribers = models.ManyToManyField(
        User,
        related_name='subscriptions',
        verbose_name='Подписчики',
        blank=True)

    class Meta():
        verbose_name = 'Блог'
        verbose_name_plural = 'Блоги'

    def __str__(self):
        return 'Блог ' + self.user.username

    def get_absolute_url(self):
        return reverse('blogs:detail', args=[self.pk])


class Post (models.Model):
    blog = models.ForeignKey(
        Blog,
        on_delete=models.CASCADE,
        related_name='posts')
    title = models.CharField('Заголовок', max_length=255)
    body = models.TextField('Пост')
    created = models.DateTimeField('создан', auto_now_add=True)
    updated = models.DateTimeField('обновлен', blank=True, null=True)
    users = models.ManyToManyField(
        User, blank=True, help_text='Пользователи, прочитавшие пост')

    class Meta():
        ordering = ['-created']
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def save(self, *args, **kwargs):
        super(Post, self).save(*args, **kwargs)
        subscribers = Blog.objects.values_list(
            'subscribers', flat=True).filter(
            pk=self.blog.id)
        datalist = []
        for sub in subscribers:
            try:
                us = User.objects.get(id=sub)
                if us.email:
                    data = ('Новый пост в блоге, на который Вы подписаны',
                            self.title + '' + self.body + ' ' + self.get_absolute_url(),
                            'from@example.com',
                            [us.email])
                    datalist.append(data)
            except User.DoesNotExist:
                us = None
            send_mass_mail(tuple(datalist))

    def get_absolute_url(self):
        return reverse('blogs:post-detail', args=[self.pk])

    def __str__(self):
        return 'Пост в блоге ' + self.blog.user.username


class Comment (models.Model):
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name='comments',
        verbose_name='Пост')
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='user_comments',
        verbose_name='Пользователь')
    body = models.TextField('Комментарий')
    created = models.DateTimeField('создан', auto_now_add=True)

    class Meta():
        ordering = ['-created']
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return self.body
