# coding: utf-8
from django import forms
from .models import Comment
from django.forms import ModelForm


class AddCommentForm (ModelForm):
    body = forms.CharField(
        label='Комментарий',
        widget=forms.Textarea(
            attrs={
                'rows': '4',
                'cols': '75'}))

    class Meta():
        model = Comment
        fields = ['body']
